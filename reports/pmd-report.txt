src/application/Application.java:7:	All methods are static.  Consider using a utility class instead. Alternatively, you could add a private constructor or make the class abstract to silence this warning.
src/application/Application.java:9:	Parameter 'args' is not assigned and could be declared final
src/application/Application.java:10:	Local variable 'view' could be declared final
src/controller/ControllerImpl.java:35:	A class which only has private constructors should be final
src/controller/ControllerImpl.java:107:	Parameter 'property' is not assigned and could be declared final
src/controller/ControllerImpl.java:113:	Local variable 'numProperties' could be declared final
src/controller/ControllerImpl.java:185:	Local variable 'tmpPtoperty' could be declared final
src/controller/ControllerImpl.java:193:	Local variable 'consequence' could be declared final
src/controller/ControllerImpl.java:195:	Local variable 'tile' could be declared final
src/controller/ControllerImpl.java:215:	Local variable 'cardEffect' could be declared final
src/controller/ControllerImpl.java:235:	Parameter 'isTurnEnded' is not assigned and could be declared final
src/controller/ControllerImpl.java:252:	Parameter 'property' is not assigned and could be declared final
src/controller/ControllerImpl.java:257:	Parameter 'minimumExpense' is not assigned and could be declared final
src/controller/ControllerImpl.java:257:	Parameter 'player' is not assigned and could be declared final
src/controller/DialogController.java:24:	A class which only has private constructors should be final
src/controller/DialogController.java:43:	Parameter 'model' is not assigned and could be declared final
src/controller/DialogController.java:43:	Parameter 'sound' is not assigned and could be declared final
src/controller/DialogController.java:43:	Parameter 'view' is not assigned and could be declared final
src/controller/DialogController.java:65:	Local variable 'moneyAmount' could be declared final
src/controller/DialogController.java:120:	Parameter 'list' is not assigned and could be declared final
src/controller/DialogController.java:211:	Parameter 'playerName' is not assigned and could be declared final
src/controller/DialogController.java:216:	Parameter 'propertyName' is not assigned and could be declared final
src/controller/DialogController.java:221:	Parameter 'moneyAmount' is not assigned and could be declared final
src/controller/DialogController.java:221:	Parameter 'player' is not assigned and could be declared final
src/controller/Movement.java:17:	Perhaps 'transition' could be replaced by a local variable.
src/controller/Movement.java:21:	Parameter 'icon' is not assigned and could be declared final
src/controller/Movement.java:26:	Parameter 'path' is not assigned and could be declared final
src/model/BoardImpl.java:11:	Avoid unused imports such as 'model.tiles'
src/model/BoardImpl.java:24:	Private field 'gameBoard' could be made final; it is only initialized in the declaration or constructor.
src/model/BoardImpl.java:25:	Private field 'mode' could be made final; it is only initialized in the declaration or constructor.
src/model/BoardImpl.java:30:	Overridable method 'getModeGame' called during object construction
src/model/BoardImpl.java:34:	Parameter 'predicate' is not assigned and could be declared final
src/model/CardEffectSupplier.java:13:	A class which only has private constructors should be final
src/model/CardEffectSupplier.java:15:	The field name indicates a constant but its modifiers do not
src/model/CardEffectSupplier.java:15:	Variables should start with a lowercase character, 'SINGLETONSUPPLIER' starts with uppercase character.
src/model/CardEffectSupplier.java:16:	Private field 'unexpected' could be made final; it is only initialized in the declaration or constructor.
src/model/CardEffectSupplier.java:17:	Private field 'probability' could be made final; it is only initialized in the declaration or constructor.
src/model/CardEffectSupplier.java:18:	Avoid using redundant field initializer for 'done'
src/model/CardEffectSupplier.java:20:	Parameter 'probability' is not assigned and could be declared final
src/model/CardEffectSupplier.java:20:	Parameter 'unexpected' is not assigned and could be declared final
src/model/CardEffectSupplier.java:29:	Singleton is not thread safe
src/model/CareMementoTaker.java:8:	A class which only has private constructors should be final
src/model/ConcrateConsequences.java:18:	Private field 'consequences' could be made final; it is only initialized in the declaration or constructor.
src/model/ConcrateConsequences.java:19:	Private field 'textConsequences' could be made final; it is only initialized in the declaration or constructor.
src/model/ConcrateConsequences.java:20:	Private field 'values' could be made final; it is only initialized in the declaration or constructor.
src/model/Dice.java:11:	A class which only has private constructors should be final
src/model/Dice.java:14:	Only variables that are final should contain underscores (except for underscores in standard prefix/suffix), 'DICE_SINGLETON' is not final.
src/model/Dice.java:14:	The field name indicates a constant but its modifiers do not
src/model/Dice.java:14:	Variables should start with a lowercase character, 'DICE_SINGLETON' starts with uppercase character.
src/model/Dice.java:16:	Private field 'random' could be made final; it is only initialized in the declaration or constructor.
src/model/GameInitializer.java:56:	Local variable 'v' could be declared final
src/model/GameInitializer.java:93:	Consider simply returning the value vs storing it in local variable 'model'
src/model/ModelImpl.java:49:	Avoid declaring a variable if it is unreferenced before a possible exit point.
src/model/ModelImpl.java:87:	Parameter 'player' is not assigned and could be declared final
src/model/ModelImpl.java:100:	Parameter 'value' is not assigned and could be declared final
src/model/ModelImpl.java:110:	Parameter 'condition' is not assigned and could be declared final
src/model/ModelImpl.java:110:	Parameter 'tile' is not assigned and could be declared final
src/model/ModelImpl.java:110:	Parameter 'value' is not assigned and could be declared final
src/model/ModelImpl.java:157:	Parameter 'value' is not assigned and could be declared final
src/model/ModelImpl.java:168:	Parameter 'withFee' is not assigned and could be declared final
src/model/ModelImpl.java:194:	Parameter 'player' is not assigned and could be declared final
src/model/ModelImpl.java:194:	Parameter 'property' is not assigned and could be declared final
src/model/ModelImpl.java:242:	Parameter 'position' is not assigned and could be declared final
src/model/ResourceManager.java:22:	A class which only has private constructors should be final
src/model/ResourceManager.java:42:	The String literal "ERROR" appears 4 times in this file; the first occurrence is on line 42
src/model/TurnImpl.java:18:	Private field 'turnManagement' could be made final; it is only initialized in the declaration or constructor.
src/model/TurnImpl.java:19:	Private field 'loserPlayer' could be made final; it is only initialized in the declaration or constructor.
src/model/TurnImpl.java:20:	Private field 'jailMap' could be made final; it is only initialized in the declaration or constructor.
src/model/TurnImpl.java:24:	Parameter 'players' is not assigned and could be declared final
src/model/TurnImpl.java:64:	Parameter 'player' is not assigned and could be declared final
src/model/TurnImpl.java:86:	Parameter 'areSame' is not assigned and could be declared final
src/model/exceptions/NotEnoughMoneyException.java:7:	Avoid unused constructor parameters such as 'moneyAmount'.
src/model/exceptions/NotEnoughMoneyException.java:7:	Parameter 'moneyAmount' is not assigned and could be declared final
src/model/player/Player.java:3:	Avoid unused imports such as 'model.tiles'
src/model/player/PlayerInfo.java:7:	Avoid unused imports such as 'model.tiles'
src/model/player/RealPlayer.java:24:	Private field 'playersProperties' could be made final; it is only initialized in the declaration or constructor.
src/model/player/RealPlayer.java:26:	Private field 'iconPath' could be made final; it is only initialized in the declaration or constructor.
src/model/player/RealPlayer.java:39:	Parameter 'money' is not assigned and could be declared final
src/model/player/RealPlayer.java:97:	Parameter 'moneyAmount' is not assigned and could be declared final
src/model/tiles/BuildableImpl.java:14:	Private field 'rents' could be made final; it is only initialized in the declaration or constructor.
src/model/tiles/BuildableImpl.java:48:	Parameter 'buildingsNr' is not assigned and could be declared final
src/model/tiles/NotObtainableImpl.java:10:	Private field 'positionTile' could be made final; it is only initialized in the declaration or constructor.
src/model/tiles/NotObtainableImpl.java:11:	Private field 'tileType' could be made final; it is only initialized in the declaration or constructor.
src/model/tiles/NotObtainableImpl.java:30:	Parameter 'nameTile' is not assigned and could be declared final
src/model/tiles/NotObtainableImpl.java:45:	Parameter 'consequence' is not assigned and could be declared final
src/model/tiles/ObtainableImpl.java:17:	Private field 'positionTile' could be made final; it is only initialized in the declaration or constructor.
src/model/tiles/ObtainableImpl.java:19:	Private field 'price' could be made final; it is only initialized in the declaration or constructor.
src/model/tiles/ObtainableImpl.java:20:	Private field 'mortgage' could be made final; it is only initialized in the declaration or constructor.
src/model/tiles/ObtainableImpl.java:21:	Field hasMortgage has the same name as a method
src/model/tiles/ObtainableImpl.java:23:	Private field 'titeType' could be made final; it is only initialized in the declaration or constructor.
src/model/tiles/ObtainableImpl.java:37:	Use explicit scoping instead of the default package private level
src/model/tiles/ObtainableImpl.java:45:	Parameter 'nameTile' is not assigned and could be declared final
src/model/tiles/ObtainableImpl.java:80:	Parameter 'owner' is not assigned and could be declared final
src/model/tiles/Rents.java:22:	Private field 'rentsManagement' could be made final; it is only initialized in the declaration or constructor.
src/model/tiles/Rents.java:26:	Parameter 'record' is not assigned and could be declared final
src/model/tiles/Rents.java:67:	Parameter 'buildingsNr' is not assigned and could be declared final
src/test/GameInitTest.java:78:	Local variable 'playerToRemove' could be declared final
src/test/MementoTest.java:31:	Method names should not start with capital letters
src/test/MementoTest.java:39:	Local variable 'playerList' could be declared final
src/test/MementoTest.java:57:	Local variable 'nameList' could be declared final
src/test/MementoTest.java:58:	Local variable 'iconList' could be declared final
src/test/MementoTest.java:63:	Local variable 'currPlayer' could be declared final
src/test/PlayerTest.java:22:	Parameter 'buildable' is not assigned and could be declared final
src/test/PlayerTest.java:22:	Parameter 'color' is not assigned and could be declared final
src/test/PlayerTest.java:22:	Parameter 'value' is not assigned and could be declared final
src/test/PlayerTest.java:34:	Local variable 'player1' could be declared final
src/test/PlayerTest.java:38:	Local variable 'bProperty1' could be declared final
src/test/PlayerTest.java:39:	Local variable 'nbProperty1' could be declared final
src/test/PlayerTest.java:54:	Local variable 'player2' could be declared final
src/utilities/FontFactory.java:7:	All methods are static.  Consider using a utility class instead. Alternatively, you could add a private constructor or make the class abstract to silence this warning.
src/utilities/IconLoader.java:10:	The field name indicates a constant but its modifiers do not
src/utilities/IconLoader.java:10:	Variables should start with a lowercase character, 'ICONS' starts with uppercase character.
src/utilities/Pair.java:8:	Parameter 'x' is not assigned and could be declared final
src/utilities/Pair.java:8:	Parameter 'y' is not assigned and could be declared final
src/utilities/Pair.java:36:	Parameter 'obj' is not assigned and could be declared final
src/utilities/Pair.java:37:	Avoid using if statements without curly braces
src/utilities/Pair.java:39:	Avoid using if statements without curly braces
src/utilities/Pair.java:41:	Avoid using if statements without curly braces
src/utilities/Pair.java:43:	Local variable 'other' could be declared final
src/utilities/Pair.java:45:	Avoid using if statements without curly braces
src/utilities/Pair.java:47:	Avoid using if statements without curly braces
src/utilities/Pair.java:50:	Avoid using if statements without curly braces
src/utilities/Pair.java:52:	Avoid using if statements without curly braces
src/utilities/PaneDimensionSetting.java:5:	A class which only has private constructors should be final
src/utilities/PaneDimensionSetting.java:9:	Variables that are final and static should be all capitals, 'CommandBridgeWIDTH' is not all capitals.
src/utilities/PaneDimensionSetting.java:10:	Variables that are final and static should be all capitals, 'CommandBridgeHEIGHT' is not all capitals.
src/utilities/PaneDimensionSetting.java:11:	Variables that are final and static should be all capitals, 'GamePaneDIMENSION' is not all capitals.
src/utilities/PaneDimensionSetting.java:12:	Variables that are final and static should be all capitals, 'LateralPaneWIDTH' is not all capitals.
src/utilities/PaneDimensionSetting.java:13:	Variables that are final and static should be all capitals, 'LateralPaneHEIGHT' is not all capitals.
src/utilities/PaneDimensionSetting.java:15:	The field name indicates a constant but its modifiers do not
src/utilities/PaneDimensionSetting.java:15:	Variables should start with a lowercase character, 'PANEDIMENSION' starts with uppercase character.
src/utilities/Parse.java:11:	Avoid unused imports such as 'model.tiles'
src/utilities/Parse.java:30:	Local variable 'record' could be declared final
src/utilities/Parse.java:32:	Local variable 'positionTile' could be declared final
src/utilities/Parse.java:33:	Local variable 'price' could be declared final
src/utilities/Parse.java:34:	Local variable 'mortgage' could be declared final
src/utilities/Parse.java:35:	Local variable 'rents' could be declared final
src/utilities/Parse.java:44:	Local variable 'record' could be declared final
src/utilities/Parse.java:46:	Local variable 'positionTile' could be declared final
src/utilities/Parse.java:47:	Local variable 'price' could be declared final
src/utilities/Parse.java:48:	Local variable 'mortgage' could be declared final
src/utilities/Parse.java:61:	Local variable 'record' could be declared final
src/utilities/Parse.java:67:	Local variable 'record' could be declared final
src/utilities/ReadFile.java:16:	All methods are static.  Consider using a utility class instead. Alternatively, you could add a private constructor or make the class abstract to silence this warning.
src/utilities/ReadFile.java:27:	Parameter 'pathfile' is not assigned and could be declared final
src/utilities/ReadFile.java:28:	Local variable 'is' could be declared final
src/utilities/enumerations/ClassicType.java:56:	Parameter 'mode' is not assigned and could be declared final
src/utilities/enumerations/ClassicType.java:378:	Local variable 'retMap' could be declared final
src/utilities/enumerations/ClassicType.java:380:	Local variable 'avatar' could be declared final
src/utilities/enumerations/Direction.java:23:	Parameter 'player' is not assigned and could be declared final
src/utilities/enumerations/Direction.java:23:	Parameter 'position' is not assigned and could be declared final
src/utilities/enumerations/Direction.java:33:	Parameter 'player' is not assigned and could be declared final
src/utilities/enumerations/Direction.java:33:	Parameter 'position' is not assigned and could be declared final
src/utilities/enumerations/Direction.java:43:	Parameter 'player' is not assigned and could be declared final
src/utilities/enumerations/Direction.java:43:	Parameter 'position' is not assigned and could be declared final
src/utilities/enumerations/Direction.java:53:	Parameter 'player' is not assigned and could be declared final
src/utilities/enumerations/Direction.java:53:	Parameter 'position' is not assigned and could be declared final
src/utilities/enumerations/Direction.java:63:	Parameter 'player' is not assigned and could be declared final
src/utilities/enumerations/Direction.java:63:	Parameter 'position' is not assigned and could be declared final
src/utilities/enumerations/Direction.java:74:	Parameter 'player' is not assigned and could be declared final
src/utilities/enumerations/Direction.java:74:	Parameter 'position' is not assigned and could be declared final
src/utilities/enumerations/Direction.java:85:	Parameter 'player' is not assigned and could be declared final
src/utilities/enumerations/Direction.java:85:	Parameter 'position' is not assigned and could be declared final
src/utilities/enumerations/Direction.java:96:	Parameter 'player' is not assigned and could be declared final
src/utilities/enumerations/Direction.java:96:	Parameter 'position' is not assigned and could be declared final
src/utilities/enumerations/InitialDistribution.java:25:	Parameter 'contractNumber' is not assigned and could be declared final
src/utilities/enumerations/InitialDistribution.java:25:	Parameter 'moneyAmount' is not assigned and could be declared final
src/utilities/enumerations/InitialDistribution.java:25:	Parameter 'playersNumber' is not assigned and could be declared final
src/view/AlertFactory.java:13:	All methods are static.  Consider using a utility class instead. Alternatively, you could add a private constructor or make the class abstract to silence this warning.
src/view/AlertFactory.java:25:	Local variable 'choice' could be declared final
src/view/CommandBridge.java:12:	A class which only has private constructors should be final
src/view/GamePane.java:33:	No need to import a type that lives in the same package
src/view/GamePane.java:39:	A class which only has private constructors should be final
src/view/GamePane.java:45:	Avoid using redundant field initializer for 'GAMEPANE'
src/view/GamePane.java:45:	It is somewhat confusing to have a field name matching the declaring class name
src/view/GamePane.java:45:	The field name indicates a constant but its modifiers do not
src/view/GamePane.java:45:	Variables should start with a lowercase character, 'GAMEPANE' starts with uppercase character.
src/view/GamePane.java:47:	Private field 'mainPane' could be made final; it is only initialized in the declaration or constructor.
src/view/GamePane.java:49:	Private field 'iconMap' could be made final; it is only initialized in the declaration or constructor.
src/view/GamePane.java:69:	Local variable 'pane' could be declared final
src/view/GamePane.java:73:	Local variable 'icon' could be declared final
src/view/GamePane.java:88:	Local variable 'backGround' could be declared final
src/view/GamePane.java:94:	Local variable 'boardLayer' could be declared final
src/view/GamePane.java:110:	Parameter 'limit' is not assigned and could be declared final
src/view/GamePane.java:110:	Parameter 'rotate' is not assigned and could be declared final
src/view/GamePane.java:110:	Parameter 'skip' is not assigned and could be declared final
src/view/GamePane.java:111:	Local variable 'pane' could be declared final
src/view/GamePane.java:159:	Avoid reassigning parameters such as 'movement'
src/view/GamePane.java:159:	Avoid reassigning parameters such as 'movement'
src/view/GamePane.java:160:	Local variable 'tempIcon' could be declared final
src/view/GamePane.java:163:	Local variable 'path' could be declared final
src/view/GamePane.java:189:	Local variable 'control' could be declared final
src/view/GamePane.java:194:	Local variable 'tempIcon' could be declared final
src/view/GamePane.java:195:	Local variable 'path' could be declared final
src/view/GamePane.java:204:	Local variable 'control' could be declared final
src/view/GamePane.java:218:	Parameter 'pos' is not assigned and could be declared final
src/view/GamePane.java:223:	Singleton is not thread safe
src/view/GamePane.java:232:	Local variable 'contractList' could be declared final
src/view/InitialWindows.java:23:	Avoid unnecessary constructors - the compiler will generate these for you
src/view/InitialWindows.java:23:	Document empty constructor
src/view/LeftPlayersPane.java:25:	A class which only has private constructors should be final
src/view/LeftPlayersPane.java:34:	Possible unsafe assignment to a non-final static field in a constructor.
src/view/LeftPlayersPane.java:56:	Local variable 'playerList' could be declared final
src/view/Pawn.java:16:	Parameter 'path' is not assigned and could be declared final
src/view/Pawn.java:16:	Parameter 'position' is not assigned and could be declared final
src/view/Pawn.java:46:	Parameter 'coordinates' is not assigned and could be declared final
src/view/Pawn.java:58:	Parameter 'position' is not assigned and could be declared final
src/view/RightInormationPane.java:26:	A class which only has private constructors should be final
src/view/RightInormationPane.java:39:	Possible unsafe assignment to a non-final static field in a constructor.
src/view/RightInormationPane.java:40:	Possible unsafe assignment to a non-final static field in a constructor.
src/view/RightInormationPane.java:41:	Possible unsafe assignment to a non-final static field in a constructor.
src/view/RightInormationPane.java:42:	Possible unsafe assignment to a non-final static field in a constructor.
src/view/RightInormationPane.java:43:	Possible unsafe assignment to a non-final static field in a constructor.
src/view/RightInormationPane.java:44:	Possible unsafe assignment to a non-final static field in a constructor.
src/view/RightInormationPane.java:45:	Possible unsafe assignment to a non-final static field in a constructor.
src/view/RightInormationPane.java:46:	Possible unsafe assignment to a non-final static field in a constructor.
src/view/RightInormationPane.java:47:	Possible unsafe assignment to a non-final static field in a constructor.
src/view/RightInormationPane.java:152:	Parameter 'leftResult' is not assigned and could be declared final
src/view/RightInormationPane.java:152:	Parameter 'rightResult' is not assigned and could be declared final
src/view/RightInormationPane.java:179:	Parameter 'done' is not assigned and could be declared final
src/view/RightInormationPane.java:189:	Parameter 'isJail' is not assigned and could be declared final
src/view/ViewImpl.java:15:	Parameter 'exitDice' is not assigned and could be declared final
src/view/ViewImpl.java:20:	Parameter 'diceThrown' is not assigned and could be declared final
src/view/comboBoxCell/ShapeCell.java:1:	Package name contains upper case characters
src/view/comboBoxCell/ShapeCell.java:13:	Parameter 'iconPath' is not assigned and could be declared final
src/view/comboBoxCell/ShapeCell.java:21:	Parameter 'empty' is not assigned and could be declared final
src/view/comboBoxCell/ShapeCell.java:21:	Parameter 'item' is not assigned and could be declared final
src/view/comboBoxCell/ShapeCellFactory.java:1:	Package name contains upper case characters
src/view/comboBoxCell/ShapeCellFactory.java:19:	Parameter 'imageMap' is not assigned and could be declared final
src/view/comboBoxCell/ShapeCellFactory.java:27:	Parameter 'listview' is not assigned and could be declared final
src/view/gameDialog/AuctionDialog.java:1:	Package name contains upper case characters
src/view/gameDialog/AuctionDialog.java:31:	A class which only has private constructors should be final
src/view/gameDialog/AuctionDialog.java:76:	Local variable 'pw' could be declared final
src/view/gameDialog/CardDialog.java:1:	Package name contains upper case characters
src/view/gameDialog/CardDialog.java:33:	A class which only has private constructors should be final
src/view/gameDialog/CardDialog.java:170:	Local variable 'tooltip' could be declared final
src/view/gameDialog/CardDialog.java:202:	Use opposite operator instead of the logic complement operator.
src/view/gameDialog/CardDialog.java:207:	Use opposite operator instead of the logic complement operator.
src/view/gameDialog/Contract.java:1:	Package name contains upper case characters
src/view/gameDialog/Contract.java:46:	Parameter 'property' is not assigned and could be declared final
src/view/gameDialog/Contract.java:72:	Parameter 'property' is not assigned and could be declared final
src/view/gameDialog/Contract.java:88:	No need to call String.valueOf to append to a string.
src/view/gameDialog/Contract.java:95:	No need to call String.valueOf to append to a string.
src/view/gameDialog/Contract.java:96:	No need to call String.valueOf to append to a string.
src/view/gameDialog/Contract.java:97:	No need to call String.valueOf to append to a string.
src/view/gameDialog/Contract.java:100:	Parameter 'property' is not assigned and could be declared final
src/view/gameDialog/Contract.java:124:	Avoid unused method parameters such as 'insidePane'.
src/view/gameDialog/Contract.java:124:	Parameter 'colspan' is not assigned and could be declared final
src/view/gameDialog/Contract.java:124:	Parameter 'insidePane' is not assigned and could be declared final
src/view/gameDialog/Contract.java:124:	Parameter 'leftLabel' is not assigned and could be declared final
src/view/gameDialog/Contract.java:124:	Parameter 'rightLabel' is not assigned and could be declared final
src/view/gameDialog/Dialog.java:4:	Package name contains upper case characters
src/view/gameDialog/Dialog.java:156:	Consider simply returning the value vs storing it in local variable 'background'
src/view/gameDialog/MortgageDialog.java:1:	Package name contains upper case characters
src/view/gameDialog/MortgageDialog.java:26:	A class which only has private constructors should be final
src/view/gameDialog/MortgageDialog.java:74:	No need to call String.valueOf to append to a string.
src/view/gameDialog/MortgageDialog.java:75:	No need to call String.valueOf to append to a string.
src/view/gameDialog/MortgageDialog.java:121:	Parameter 'obtainedMoney' is not assigned and could be declared final
src/view/gameDialog/MortgageDialog.java:122:	No need to call String.valueOf to append to a string.
src/view/gameDialog/PlayerContractsListView.java:1:	Package name contains upper case characters
src/view/gameDialog/PlayerContractsListView.java:26:	Document empty constructor
src/view/gameDialog/PlayerContractsListView.java:39:	Local variable 'property' could be declared final
src/view/gameDialog/PlayerContractsListView.java:50:	Parameter 'paint' is not assigned and could be declared final
src/view/gameDialog/PlayerContractsListView.java:50:	Parameter 'text' is not assigned and could be declared final
src/view/gameDialog/PlayerContractsListView.java:52:	Local variable 'tmpPaint' could be declared final
src/view/gameDialog/SettingsDialog.java:1:	Package name contains upper case characters
src/view/gameDialog/SettingsDialog.java:15:	A class which only has private constructors should be final
src/view/gameDialog/SettingsDialog.java:35:	Parameter 'sound' is not assigned and could be declared final
src/view/gameDialog/TradeDialog.java:1:	Package name contains upper case characters
src/view/gameDialog/TradeDialog.java:32:	A class which only has private constructors should be final
src/view/gameSetup/MainMenu.java:1:	Package name contains upper case characters
src/view/gameSetup/MainMenu.java:52:	The String literal "mainMenuButton" appears 4 times in this file; the first occurrence is on line 52
src/view/gameSetup/MainMenu.java:181:	Parameter 'stage' is not assigned and could be declared final
src/view/gameSetup/PlayerSetupBox.java:1:	Package name contains upper case characters
src/view/gameSetup/PlayerSetupBox.java:15:	Avoid unused imports such as 'view.comboBoxCell'
src/view/gameSetup/PlayerSetupBox.java:25:	Parameter 'imageMap' is not assigned and could be declared final
src/view/gameSetup/PlayerSetupMenu.java:1:	Package name contains upper case characters
src/view/gameSetup/PlayerSetupMenu.java:28:	A class which only has private constructors should be final
src/view/gameSetup/PlayerSetupMenu.java:38:	Private field 'iconList' could be made final; it is only initialized in the declaration or constructor.
src/view/gameSetup/PlayerSetupMenu.java:39:	Private field 'chosenList' could be made final; it is only initialized in the declaration or constructor.
src/view/gameSetup/PlayerSetupMenu.java:40:	Private field 'imageMap' could be made final; it is only initialized in the declaration or constructor.
src/view/gameSetup/PlayerSetupMenu.java:130:	Parameter 'flowPane' is not assigned and could be declared final
src/view/gameSetup/PlayerSetupMenu.java:131:	Local variable 'pBox' could be declared final
src/view/gameSetup/PlayerSetupMenu.java:174:	Parameter 'names' is not assigned and could be declared final
src/view/gameSetup/PlayerSetupMenu.java:175:	Local variable 'name' could be declared final
src/view/gameSetup/PlayerSetupMenu.java:183:	Parameter 'icons' is not assigned and could be declared final
src/view/gameSetup/PlayerSetupMenu.java:184:	Local variable 'icon' could be declared final
src/view/gameSetup/PlayerSetupMenu.java:192:	Parameter 'stage' is not assigned and could be declared final
src/view/handlers/HandleFileChooser.java:28:	Parameter 'e' is not assigned and could be declared final
src/view/tiles/ComponentFactory.java:20:	All methods are static.  Consider using a utility class instead. Alternatively, you could add a private constructor or make the class abstract to silence this warning.
src/view/tiles/ComponentFactory.java:23:	Variables that are final and static should be all capitals, 'LandSimpleWIDTH' is not all capitals.
src/view/tiles/ComponentFactory.java:24:	Variables that are final and static should be all capitals, 'LandHEIGHT' is not all capitals.
src/view/tiles/ComponentFactory.java:25:	Variables that are final and static should be all capitals, 'LandCornerDimension' is not all capitals.
src/view/tiles/ComponentFactory.java:28:	Local variable 'landPane' could be declared final
src/view/tiles/ComponentFactory.java:46:	Local variable 'colorFamily' could be declared final
src/view/tiles/ComponentFactory.java:54:	Local variable 'label' could be declared final
src/view/tiles/ComponentFactory.java:69:	Local variable 'image' could be declared final
src/view/tiles/ComponentFactory.java:70:	Local variable 'imageView' could be declared final
src/view/tiles/ComponentFactory.java:82:	Local variable 'seperator' could be declared final
src/view/tiles/ComponentFactory.java:88:	Parameter 'node' is not assigned and could be declared final
src/view/tiles/LandAbstractFactory.java:39:	Local variable 'landPane' could be declared final
src/view/tiles/LandAbstractFactory.java:49:	Local variable 'colorFamily' could be declared final
src/view/tiles/LandAbstractFactory.java:50:	Local variable 'seperator' could be declared final
src/view/tiles/LandAbstractFactory.java:52:	Local variable 'textHeader' could be declared final
src/view/tiles/LandAbstractFactory.java:53:	Local variable 'textRent' could be declared final
src/view/tiles/LandAbstractFactory.java:64:	Local variable 'landPane' could be declared final
src/view/tiles/LandAbstractFactory.java:74:	Local variable 'top' could be declared final
src/view/tiles/LandAbstractFactory.java:75:	Local variable 'image' could be declared final
src/view/tiles/LandAbstractFactory.java:76:	Local variable 'bottom' could be declared final
src/view/tiles/LandAbstractFactory.java:76:	No need to call String.valueOf to append to a string.
src/view/tiles/LandAbstractFactory.java:95:	Local variable 'temp' could be declared final
src/view/tiles/LandAbstractFactory.java:97:	Local variable 'top' could be declared final
src/view/tiles/LandAbstractFactory.java:98:	Local variable 'image' could be declared final
src/view/tiles/LandAbstractFactory.java:101:	Local variable 'bottom' could be declared final
